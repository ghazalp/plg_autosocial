
   <?php
// The general information at the top of each file
/**
* @version              $Id$
* @package              Joomla
* @copyright    Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
* @license              GNU General Public License, see LICENSE.php
*/
 
// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Import Joomla! Plugin library file
jimport('joomla.plugin.plugin');
 
//The Content plugin Loadmodule
class plgContentAutosocial extends JPlugin {

   

      public function onContentAfterSave($context, &$article, $isNew)
   {
      
      // Paramètres Twitter
   $consumerKey    = $this->params->get('twitter_ConsumerKey');
   $consumerSecret = $this->params->get('twitter_ConsumerSecret');
   $oAuthToken     = $this->params->get('twitter_oauthToken');
   $oAuthSecret     = $this->params->get('twitter_oauthSecret');
   $articleId = $article->id;


   //include ('twitteroauth/OAuth.php');
   
   require_once JPATH_ROOT . '/plugins/content/autosocial/twitteroauth/OAuth.php';
   require_once JPATH_ROOT . '/plugins/content/autosocial/twitteroauth/twitteroauth.php';

      $tweet = new TwitterOAuth($consumerKey, $consumerSecret, $oAuthToken, $oAuthSecret);
      $tweetmsg = 'Article ID'.$articleId. 'Saved';
      $tweet->post('statuses/update',array('status' => $tweetmsg));


   }
 
}
  